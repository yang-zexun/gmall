package com.atguigu.gmall.cart.service;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.cart.feign.GmallPmsClient;
import com.atguigu.gmall.cart.feign.GmallSmsClient;
import com.atguigu.gmall.cart.feign.GmallWmsClient;
import com.atguigu.gmall.cart.interceptors.LoginInterceptor;
import com.atguigu.gmall.cart.pojo.Cart;
import com.atguigu.gmall.cart.pojo.UserInfo;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.common.exception.CartException;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;
import com.atguigu.gmall.pms.entity.SkuEntity;
import com.atguigu.gmall.sms.vo.ItemSaleVo;
import com.atguigu.gmall.wms.entity.WareSkuEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartService {


    private static final String KEY_PREFIX = "cart:info:";  //购物车信息前缀
    private static final String PRICE_PREFIX = "cart:price:";   //实时价格前缀

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private CartAsyncService asyncService;

    @Autowired
    private GmallPmsClient pmsClient;

    @Autowired
    private GmallSmsClient smsClient;

    @Autowired
    private GmallWmsClient wmsClient;

    public void addCart(Cart cart) {
        String userId = getUserId();
        //组装redis外层的key
        String key = KEY_PREFIX + userId;
        //获取当前用户的购物车map
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(key);
        //判断购物车里是否有该商品
        String skuId = cart.getSkuId().toString(); //要新增的skuId
        BigDecimal count = cart.getCount();  //要新增的数量
        if (hashOps.hasKey(skuId)) {
            //如果包含，则累加
            String json = hashOps.get(skuId).toString();
            cart = JSON.parseObject(json, Cart.class);
            cart.setCount(cart.getCount().add(count));
            //使用新的cart覆盖旧的cart，同时完成mysql同步（异步操作）
            this.asyncService.updateCart(userId, skuId, cart);
        } else {
            //如果不包含，新增一条购买记录
            cart.setUserId(userId);
            cart.setCheck(true);
            //查询sku相关信息
            ResponseVo<SkuEntity> responseVo = this.pmsClient.querySkuById(cart.getSkuId());
            SkuEntity skuEntity = responseVo.getData();
            if (skuEntity == null) {
                throw new CartException("此商品不存在！！！！！！！！！");
            }
            cart.setTitle(skuEntity.getTitle());
            cart.setPrice(skuEntity.getPrice());
            cart.setDefaultImage(skuEntity.getDefaultImage());

            // 查询库存
            ResponseVo<List<WareSkuEntity>> wareResponseVo = this.wmsClient.queryWareSkusBySkuId(cart.getSkuId());
            List<WareSkuEntity> wareSkuEntities = wareResponseVo.getData();
            if (!CollectionUtils.isEmpty(wareSkuEntities)) {
                cart.setStore(wareSkuEntities.stream().anyMatch(wareSkuEntity -> wareSkuEntity.getStock() - wareSkuEntity.getStockLocked() > 0));
            }

            // 销售属性
            ResponseVo<List<SkuAttrValueEntity>> responseVo1 = this.pmsClient.querySaleAttrValuesBySkuId(cart.getSkuId());
            List<SkuAttrValueEntity> skuAttrValueEntities = responseVo1.getData();
            cart.setSaleAttrs(JSON.toJSONString(skuAttrValueEntities));

            // 营销信息
            ResponseVo<List<ItemSaleVo>> salesResponseVo = this.smsClient.querySalesBySkuId(cart.getSkuId().toString());
            List<ItemSaleVo> itemSaleVos = salesResponseVo.getData();
            cart.setSales(JSON.toJSONString(itemSaleVos));

            // 新增到数据库
            this.asyncService.insertCart(userId.toString(),cart);

            //在redis存一份实时的价格
            this.redisTemplate.opsForValue().set(PRICE_PREFIX + skuId, skuEntity.getPrice().toString());
        }
        // 保存新数据到redis
        hashOps.put(skuId, JSON.toJSONString(cart));
    }
    //添加完购物车回显用的
    public Cart queryCartBySkuId(Long skuId) {
        //获取登录用户信息
        String userId = this.getUserId();
        //获取当前用户的购物车操作对象
        BoundHashOperations<String, Object, Object> hashMap = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        String json = hashMap.get(skuId.toString()).toString();
        if (StringUtils.isBlank(json)) {
            throw new CartException("对应的购物车记录不存在！！！！！！！！");
        }

        return JSON.parseObject(json, Cart.class);
    }

    //查看我的购物车
    public List<Cart> queryCarts() {
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        // 查询未登录的购物车，相当于拿到内存的map
        BoundHashOperations<String, Object, Object> unLoginHashmap = this.redisTemplate.boundHashOps(KEY_PREFIX + userInfo.getUserKey());
        // 获取未登录用户的购物车json字符串集合
        List<Object> cartJsons = unLoginHashmap.values();
        // 如果未登录的购物车不是空，则转为购物车集合 购物车里装着一条一条的(skuid--->cart)
        List<Cart> unLoginCarts = null;
        if (!CollectionUtils.isEmpty(cartJsons)) {
            // 未登录的购物车集合
            // 查询实时价格缓存，可能价格已经改变，要给实时价格属性赋值
            unLoginCarts = cartJsons.stream().map(cartJson -> {
                Cart cart = JSON.parseObject(cartJson.toString(), Cart.class);
                cart.setCurrentPrice(new BigDecimal(this.redisTemplate.opsForValue().get(PRICE_PREFIX + cart.getSkuId())));
                return cart;
            }).collect(Collectors.toList());
        }

        // 2.获取登录信息，判断是否登录（userId=null说明没有登录），没有登录则直接返回未登录的购物车即可
        Long userId = userInfo.getUserId();
        if (userId == null) {
            return unLoginCarts;
        }

        // 3.如果登录了，需要合并未登录的购物车 到 已登录的购物车中
        BoundHashOperations<String, Object, Object> loginHashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        // 判断未登录的购物车是否为空，不为空则需要遍历未登录的购物车，然后合并到已登录的购物车中
        if (!CollectionUtils.isEmpty(unLoginCarts)) {
            unLoginCarts.forEach(cart -> {
                // 参照新增购物车：如果已登录购物车中包含了该商品 则更新数量  如果没有包含该商品则新增一条记录
                String skuId = cart.getSkuId().toString();  //未登录购物车的skuid
                BigDecimal count = cart.getCount();  //未登录购物车添加的个数
                BigDecimal currentPrice = cart.getCurrentPrice();
                if (loginHashOps.hasKey(skuId)) {
                    String cartJson = loginHashOps.get(skuId).toString();
                    cart = JSON.parseObject(cartJson, Cart.class);
                    cart.setCount(cart.getCount().add(count));
                    cart.setCurrentPrice(currentPrice);
                    // 更新到数据库
                    this.asyncService.updateCart(userId.toString(), skuId, cart);
                } else {
                    // 用userId 把userKey 给替换掉
                    cart.setUserId(userId.toString());
                    this.asyncService.insertCart(userId.toString(),cart);
                }
                loginHashOps.put(skuId, JSON.toJSONString(cart));
            });

            // 4.删除未登录的购物车
            this.redisTemplate.delete(KEY_PREFIX + userInfo.getUserKey());
            this.asyncService.deleteCartByUserId(userInfo.getUserKey());

        }
        // 5.查询已登录的购物车
        List<Object> loginCartJsons = loginHashOps.values();
        if (CollectionUtils.isEmpty(loginCartJsons)) {
            return null;
        }
        return loginCartJsons.stream().
                map(cartJson -> JSON.parseObject(cartJson.toString(), Cart.class)).collect(Collectors.toList());
    }


    /**
     * 获取userId（登录）或者userKey（未登录），决定后续添加时添加到哪个购物车
     *
     * @return
     */
    private String getUserId() {
        // 从拦截器中获取userInfo
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        // 获取userKey
        String userId = userInfo.getUserKey();
        // 如果登录了（userId不为空），用userId覆盖掉userKey
        if (userInfo.getUserId() != null) {
            userId = userInfo.getUserId().toString();
        }
        return userId;
    }

    public void updateNum(Cart cart) {
        // 获取登录状态
        String userId = this.getUserId();

        // 根据登录信息查询当前用户的购物车
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        if (!hashOps.hasKey(cart.getSkuId().toString())){
            throw new CartException("当前用户没有该条购物车记录");
        }

        // 获取当前用户对应的购物车记录
        String cartJson = hashOps.get(cart.getSkuId().toString()).toString();
        BigDecimal count = cart.getCount();
        cart = JSON.parseObject(cartJson, Cart.class);
        cart.setCount(count);

        hashOps.put(cart.getSkuId().toString(), JSON.toJSONString(cart));
        this.asyncService.updateCart(userId, cart.getSkuId().toString(), cart);
    }

    public void deleteCart(Long skuId) {
        String userId = this.getUserId();

        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);

        hashOps.delete(skuId.toString());
        this.asyncService.deleteCartByUserIdAndSkuId(userId, skuId);
    }

    public List<Cart> queryCheckedCartsByUserId(Long userId) {

        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        List<Object> cartJsons = hashOps.values();
        if (CollectionUtils.isEmpty(cartJsons)){
            return null;
        }

        return cartJsons.stream().map(cartJson -> JSON.parseObject(cartJson.toString(), Cart.class)).filter(Cart::getCheck).collect(Collectors.toList());
    }
}
