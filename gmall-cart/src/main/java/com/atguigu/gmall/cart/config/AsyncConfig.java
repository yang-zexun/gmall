package com.atguigu.gmall.cart.config;

import com.atguigu.gmall.cart.exception.handler.CartAsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;

import java.util.concurrent.Executor;

@Configuration
public class AsyncConfig implements AsyncConfigurer {

    @Autowired
    private CartAsyncUncaughtExceptionHandler exceptionHandler;
    /**
     * 配置线程城，控制线程数
     * @return
     */
    @Override
    public Executor getAsyncExecutor() {
        return null;
    }
    /**
     *
     * @return
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return exceptionHandler;
    }
}
