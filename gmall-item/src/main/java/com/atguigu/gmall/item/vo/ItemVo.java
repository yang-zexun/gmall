package com.atguigu.gmall.item.vo;

import com.atguigu.gmall.pms.entity.CategoryEntity;
import com.atguigu.gmall.pms.entity.SkuImagesEntity;
import com.atguigu.gmall.pms.vo.GroupVo;
import com.atguigu.gmall.pms.vo.SaleAttrValueVo;
import com.atguigu.gmall.sms.vo.ItemSaleVo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
public class ItemVo {

    // 面包屑：一二三级分类（id name）  V
    private List<CategoryEntity> categories;

    // 面包屑：品牌信息 V
    private Long brandId;
    private String brandName;

    // 面包屑：spu信息 V
    private Long spuId;
    private String spuName;

    // 中间详情 sku详情  V
    private Long skuId;
    private String title;
    private String subTitle;
    private BigDecimal price;
    private Integer weight;
    private String defaultImage;

    //  商品营销信息 V
    private List<ItemSaleVo> sales;

    // sku图片列表 V
    private List<SkuImagesEntity> images;

    // 是否有货 V
    private Boolean store;

    // 销售属性列表 V
    // [{attrId: 3, attrName: 机身颜色, attrValues: ['暗夜黑', '白天白']},
    // {attrId: 4, attrName: 机身内存, attrValues: ['8G', '12G']},
    // {attrId: 5, attrName: 机身存储, attrValues: ['256G', '512G']}]
    private List<SaleAttrValueVo> saleAttrs;

    // 当前sku的销售属性: {3: '暗夜黑', 4: '12G', 5: '512G'} V
    private Map<Long, String> saleAttr;

    // 销售属性组合 和 skuId 的映射关系 V
    // {'暗夜黑, 8G, 512G': 100, '暗夜黑, 12G, 512G': 101}
    private Map<String, Long> skusJson;

    // 商品描述 V
    private List<String> spuImages;

    private List<GroupVo> groups;
}
