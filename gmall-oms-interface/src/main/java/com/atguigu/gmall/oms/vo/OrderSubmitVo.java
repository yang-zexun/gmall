package com.atguigu.gmall.oms.vo;

import com.atguigu.gmall.ums.entity.UserAddressEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderSubmitVo {

    private String orderToken; // 防重的唯一标识，将来也可以作为订单编号使用

    private UserAddressEntity address; // 用户选择的收获地址

    private Integer payType; // 支付方式

    private String deliveryCompany; // 配送方式 物流公司

    private Integer bounds; // 消费积分

    private List<OrderItemVo> items; // 送货清单

    private BigDecimal totalPrice; // 页面商品的总价格。验总价
}
