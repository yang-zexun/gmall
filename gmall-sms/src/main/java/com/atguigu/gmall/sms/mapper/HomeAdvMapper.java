package com.atguigu.gmall.sms.mapper;

import com.atguigu.gmall.sms.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 *
 * @author fengge
 * @email fengge@atguigu.com
 * @date 2021-07-31 11:16:32
 */
@Mapper
public interface HomeAdvMapper extends BaseMapper<HomeAdvEntity> {

}
