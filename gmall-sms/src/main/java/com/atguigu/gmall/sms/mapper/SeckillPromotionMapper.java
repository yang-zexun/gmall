package com.atguigu.gmall.sms.mapper;

import com.atguigu.gmall.sms.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 *
 * @author fengge
 * @email fengge@atguigu.com
 * @date 2021-07-31 11:16:32
 */
@Mapper
public interface SeckillPromotionMapper extends BaseMapper<SeckillPromotionEntity> {

}
