package com.atguigu.gmall.wms.vo;

import lombok.Data;

@Data
public class SkuLockVo {

    // 锁定商品的id
    private Long skuId;
    // 要购买的数量
    private Integer count;
    // 锁定状态：锁成功-true  锁定失败-false
    private Boolean lock = false;
    // 锁定库存的id
    private Long wareSkuId;
}
