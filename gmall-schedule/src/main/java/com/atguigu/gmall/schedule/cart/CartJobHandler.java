package com.atguigu.gmall.schedule.cart;

import com.atguigu.gmall.schedule.mapper.CartMapper;
import com.atguigu.gmall.schedule.pojo.Cart;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class CartJobHandler {

    @Autowired
    private CartMapper cartMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    private static final String EXCEPTION_KEY = "cart:exception";
    private static final String KEY_PREFIX = "cart:info:";
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @XxlJob("cartSyncData")
    public ReturnT<String> syncData(String param){

        // 读取失败用户列表[userIds]
        BoundSetOperations<String, String> setOps = this.redisTemplate.boundSetOps(EXCEPTION_KEY);
        // 获取set集合中的一个元素 并移除
        String userId = setOps.pop();

        while (userId != null) {
            /**
             * mysql同步购物车数据异常时处理方案。
             * 总体思路：把失败用户的购物车mysql全删了，再把redis读取一边重新导入mysql
             */
            // 1.删除mysql中 失败用户的购物车
            this.cartMapper.delete(new UpdateWrapper<Cart>().eq("user_id", userId));

            // 2.获取redis中的失败用户的购物车
            BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
            List<Object> cartJsons = hashOps.values();
            if (CollectionUtils.isEmpty(cartJsons)){
                continue;
            }

            // 3.遍历redis中的购物车 新增到mysql
            cartJsons.forEach(cartJson -> {
                try {
                    Cart cart = MAPPER.readValue(cartJson.toString(), Cart.class);
                    this.cartMapper.insert(cart);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });

            // 获取下一个用户的id
            userId = setOps.pop();
        }

        return ReturnT.SUCCESS;
    }
}
