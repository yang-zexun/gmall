package com.atguigu.gmall.search;

import com.atguigu.gmall.common.bean.PageParamVo;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.pms.entity.*;
import com.atguigu.gmall.search.feign.GmallPmsClient;
import com.atguigu.gmall.search.feign.GmallWmsClient;
import com.atguigu.gmall.search.pojo.Goods;
import com.atguigu.gmall.search.pojo.SearchAttrValueVo;
import com.atguigu.gmall.search.repository.GoodsRepository;
import com.atguigu.gmall.wms.entity.WareSkuEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

@SpringBootTest
class GmallSearchApplicationTests {
    @Autowired
    GmallPmsClient pmsClient;
    @Autowired
    GmallWmsClient wmsClient;
    @Autowired
    ElasticsearchRestTemplate template;
    @Autowired
    GoodsRepository goodsRepository;
    @Test
    void contextLoads() {
        //创建索引库和映射
        if (!this.template.indexExists(Goods.class)){
            this.template.createIndex(Goods.class);
            this.template.putMapping(Goods.class);
        }

        int pageNum = 1;
        int pageSize = 100;
        do {
            PageParamVo pageParamVo = new PageParamVo(pageNum, pageSize, null);
            ResponseVo<List<SpuEntity>> responseVo = this.pmsClient.querySpuByPageJson(pageParamVo);
            List<SpuEntity> data = responseVo.getData();

            // 判空，如果spu的集合为空，则直接结束
            if (CollectionUtils.isEmpty(data)){
                return;
            }

            // 遍历当前页的spu，查询每个spu下的sku集合
            data.forEach(spuEntity -> {
                ResponseVo<List<SkuEntity>> skuResponse = this.pmsClient.getSkuBySpuId(spuEntity.getId());
                List<SkuEntity> skuEntities = skuResponse.getData();

                if (!CollectionUtils.isEmpty(skuEntities)){
                    //查品牌
                    ResponseVo<BrandEntity> brandResponseVo = this.pmsClient.queryBrandById(spuEntity.getBrandId());
                    BrandEntity brandEntity = brandResponseVo.getData();

                    //查分类对象
                    ResponseVo<CategoryEntity> categoryResponseVo = this.pmsClient.queryCategoryById(spuEntity.getCategoryId());
                    CategoryEntity categoryEntity = categoryResponseVo.getData();

                    //把sku集合转换为goods集合
                    List<Goods> goodsList = skuEntities.stream().map(skuEntity -> {
                        Goods goods = new Goods();
                        //sku信息
                        goods.setSkuId(skuEntity.getId());
                        goods.setDefaultImage(skuEntity.getDefaultImage());
                        goods.setTitle(skuEntity.getTitle());
                        goods.setSubTitle(skuEntity.getSubtitle());
                        goods.setPrice(skuEntity.getPrice().doubleValue());

                        //排序信息
                        //销量+库存
                        ResponseVo<List<WareSkuEntity>> wareSkus = this.wmsClient.queryWareSkusBySkuId(skuEntity.getId());
                        List<WareSkuEntity> wareSkusData = wareSkus.getData();
                        if (!CollectionUtils.isEmpty(wareSkusData)){
                            goods.setSales(wareSkusData.stream().map(WareSkuEntity::getSales).reduce((a,b)->a+b).get());
                            goods.setStore(wareSkusData.stream().anyMatch(wareSkuEntity -> wareSkuEntity.getStock() - wareSkuEntity.getStockLocked() > 0));
                        }

                        //品牌信息
                        if (brandEntity != null){
                            goods.setBrandId(brandEntity.getId());
                            goods.setBrandName(brandEntity.getName());
                            goods.setLogo(brandEntity.getLogo());
                        }

                        //分类相关
                        if (categoryEntity != null){
                            goods.setCategoryId(categoryEntity.getId());
                            goods.setCategoryName(categoryEntity.getName());
                        }

                        //规格参数聚合
                        //销售参数及值
                        ArrayList<SearchAttrValueVo> searchAttrs = new ArrayList<>();
                        ResponseVo<List<SkuAttrValueEntity>> attrValuesResponse = this.pmsClient.querySearchAttrValuesBySkuIdAndCid(skuEntity.getId(), skuEntity.getCategoryId());
                        List<SkuAttrValueEntity> skuAttrValues = attrValuesResponse.getData();
                        if (!CollectionUtils.isEmpty(skuAttrValues)){
                            searchAttrs.addAll(skuAttrValues.stream().map(skuAttrValueEntity -> {
                                SearchAttrValueVo searchAttrValueVo = new SearchAttrValueVo();
                                BeanUtils.copyProperties(skuAttrValueEntity, searchAttrValueVo);
                                return searchAttrValueVo;
                            }).collect(Collectors.toList()));
                        }
                        // 基本类型的检索类型的规格参数及值
                        ResponseVo<List<SpuAttrValueEntity>> spuAttrResponseVo = this.pmsClient.querySearchAttrValuesBySpuIdAndCid(spuEntity.getId(), spuEntity.getCategoryId());
                        List<SpuAttrValueEntity> spuAttrValueEntities = spuAttrResponseVo.getData();
                        if (!CollectionUtils.isEmpty(spuAttrValueEntities)){
                            searchAttrs.addAll(spuAttrValueEntities.stream().map(spuAttrValueEntity -> {
                                SearchAttrValueVo searchAttrValueVo = new SearchAttrValueVo();
                                BeanUtils.copyProperties(spuAttrValueEntity, searchAttrValueVo);
                                return searchAttrValueVo;
                            }).collect(Collectors.toList()));
                        }

                        goods.setSearchAttrs(searchAttrs);
                        return goods;
                    }).collect(Collectors.toList());

                    this.goodsRepository.saveAll(goodsList);
                }
            });

            pageSize = data.size();
            pageNum++; // 下一页
        } while (pageSize == 100);
    }

//    public static void main(String[] args) {
//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println(System.currentTimeMillis());
//            }
//        },3000,3000);
//    }
}
