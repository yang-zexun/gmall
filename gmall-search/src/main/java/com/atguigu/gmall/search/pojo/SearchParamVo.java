package com.atguigu.gmall.search.pojo;

import lombok.Data;

import java.util.List;

@Data
public class SearchParamVo {

    // 搜索的关键字
    private String keyword;

    // 品牌的过滤条件
    private List<Long> brandId;

    // 分类的过滤条件
    private List<Long> categoryId;

    // 规格参数的过滤条件：[4:8G-12G-6G, 5:128G-256G]
    private List<String> props;

    // 价格区间的过滤条件
    private Double priceFrom;
    private Double priceTo;

    // 是否有货过滤
    private Boolean store;

    // 排序条件: 默认-得分降序排列 1-价格降序 2-价格升序 3-销量降序 4-新品降序
    private Integer sort;

    // 分页参数
    private Integer pageNum = 1;
    private final Integer pageSize = 20;
}
