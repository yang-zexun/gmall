package com.atguigu.gmall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@SpringBootApplication
public class GmallGatewayApplication {

    public static void main(String[] args) {

        SpringApplication.run(GmallGatewayApplication.class, args);
    }

}
