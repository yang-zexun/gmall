package com.atguigu.gmall.gateway.filter;

import com.atguigu.gmall.common.utils.IpUtils;
import com.atguigu.gmall.common.utils.JwtUtils;
import com.atguigu.gmall.gateway.config.JwtProperties;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@EnableConfigurationProperties({JwtProperties.class})
@Component
public class AuthGatewayFilterFactory extends AbstractGatewayFilterFactory<AuthGatewayFilterFactory.PathConfig> {
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JwtProperties jwtProperties;

    public AuthGatewayFilterFactory() {
        super(PathConfig.class);
    }

    @Override
    public ShortcutType shortcutType() {
        return ShortcutType.GATHER_LIST;
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("path");
    }
    @Override
    public GatewayFilter apply(PathConfig config) {
        return new GatewayFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                //获取request对象
                ServerHttpRequest request = exchange.getRequest();
                ServerHttpResponse response = exchange.getResponse();
                try {
                    // 1.判断当前请求在不在拦截名单中，如果不在，直接放行
                    // 1.1 获取当前请求路径
                    String curPath = request.getURI().getPath();
                    // 1.2 获取拦截名单
                    List<String> paths= config.path;
                    // 1.3 如果拦截名单是空，拦截所有路径；如果不为空，判断请求在不在拦截名单中
                    if (!CollectionUtils.isEmpty(paths) && !paths.stream().anyMatch(path -> curPath.startsWith(path))){
                        return chain.filter(exchange);
                    }

                    // 2.获取请求头的中的token(先从头中拿，拿不到去cookie拿)： cookie（同步放的位置）  header（异步）
                    String token = request.getHeaders().getFirst("token");
                    if (StringUtils.isBlank(token)){
                        // 2.1 如果头中没有，则从cookie中获取
                        MultiValueMap<String, HttpCookie> cookies = request.getCookies();
                        if (!CollectionUtils.isEmpty(cookies) && cookies.containsKey(jwtProperties.getCookieName())){
                            HttpCookie cookiesFirst = cookies.getFirst(jwtProperties.getCookieName());
                            token = cookiesFirst.getValue();
                        }
                    }

                    // 3.判断token是否为空，为空则拦截， 重定向到登录页面
                    if (StringUtils.isBlank(token)){
                         response = redirectLogin(request, response);
                        return response.setComplete();
                    }

                    // 4.解析token，如果解析失败（不合格或过期修改），则重定向到登录页面
                    Map<String, Object> fromToken = JwtUtils.getInfoFromToken(token, jwtProperties.getPublicKey());

                    // 5.判断当前请求的ip地址 和 token中的ip地址是否一致，不一致则拦截，重定向到登录页面
                    String curIp = IpUtils.getIpAddressAtGateway(request);
                    String ip = fromToken.get("ip").toString();
                    if (!StringUtils.equals(curIp,ip)){
                        response = redirectLogin(request,response);
                        return response.setComplete();
                    }

                    // 6.把解析后的结果集传给后续的业务操作，因为解析jwt费劲，所以直在网关解析一次
                    request.mutate()
                            .header("userId", fromToken.get("userId").toString())
                            .header("username", fromToken.get("username").toString())
                            .build();
                    exchange.mutate().request(request).build();

                    // 7.放行
                    return chain.filter(exchange);

                } catch (Exception e) {
                    e.printStackTrace();
                    response = redirectLogin(request,response);
                    return response.setComplete();
                }
            }
        };

    }
    @Data
    static class PathConfig{
        private List<String> path;
    }

    /**
     * 重定向到登录页面
     * @param request ServerHttpRequest
     * @param response ServerHttpResponse
     * @return
     */
    private ServerHttpResponse redirectLogin(ServerHttpRequest request,ServerHttpResponse response){
        // 响应状态码：303，代表重定向
        response.setStatusCode(HttpStatus.SEE_OTHER);
        // 指定重定向的地址
        response.getHeaders().add(HttpHeaders.LOCATION, "http://sso.gmall.com/toLogin.html?returnUrl=" + request.getURI());
        return response;
    }

}
