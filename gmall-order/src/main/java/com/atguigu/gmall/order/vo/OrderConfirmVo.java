package com.atguigu.gmall.order.vo;

import com.atguigu.gmall.oms.vo.OrderItemVo;
import com.atguigu.gmall.ums.entity.UserAddressEntity;
import lombok.Data;

import java.util.List;

@Data
public class OrderConfirmVo {

    private List<UserAddressEntity> addresses; // 收获地址列表

    private List<OrderItemVo> items; // 送货清单

    private Integer bounds; // 购物积分

    // 保证提交订单幂等性
    private String orderToken; // 为了防止重复提交而设置的一个唯一性字段
}
