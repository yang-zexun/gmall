package com.atguigu.gmall.pms.service.impl;

import com.atguigu.gmall.pms.entity.AttrEntity;
import com.atguigu.gmall.pms.entity.SkuEntity;
import com.atguigu.gmall.pms.mapper.AttrMapper;
import com.atguigu.gmall.pms.mapper.SkuMapper;
import com.atguigu.gmall.pms.vo.SaleAttrValueVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.pms.mapper.SkuAttrValueMapper;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;
import com.atguigu.gmall.pms.service.SkuAttrValueService;
import org.springframework.util.CollectionUtils;


@Service("skuAttrValueService")
public class SkuAttrValueServiceImpl extends ServiceImpl<SkuAttrValueMapper, SkuAttrValueEntity> implements SkuAttrValueService {
    @Autowired
    AttrMapper attrMapper;
    @Autowired
    SkuMapper skuMapper;
    @Autowired
    SkuAttrValueMapper attrValueMapper;

    @Override
    public List<SkuAttrValueEntity> querySearchAttrValuesBySkuIdAndCid(Long skuId, Long cid) {
        //找出此分类下面的所有规格参数，同时筛选出不支持检索，只保留支持检索类型的规格参数
        List<AttrEntity> list = attrMapper.selectList(
                new QueryWrapper<AttrEntity>().eq("category_id", cid).
                                                eq("search_type",1));

        //健壮性判断
        if (CollectionUtils.isEmpty(list)){
            return null;
        }

        //拿到规格参数的id值
        List<Long> attrIdList = list.stream().map(AttrEntity::getId).collect(Collectors.toList());

        //根据skuId查询此商品对应的规格参数
        return this.list(new QueryWrapper<SkuAttrValueEntity>().eq("sku_id",skuId).in("attr_id",attrIdList));
    }

    @Override
    public List<SaleAttrValueVo> querySaleAttrValueBySpuId(Long spuId) {
        // 根据spuId查询所有sku
        List<SkuEntity> skuEntities = this.skuMapper.selectList(new QueryWrapper<SkuEntity>().eq("spu_id", spuId));
        if (CollectionUtils.isEmpty(skuEntities)){
            return null;
        }

        // 根据skuIds查询所有的销售属性
        List<Long> skuIds = skuEntities.stream().map(SkuEntity::getId).collect(Collectors.toList());
        List<SkuAttrValueEntity> skuAttrValueEntities = this.list(new QueryWrapper<SkuAttrValueEntity>().in("sku_id", skuIds).orderByAsc("attr_id"));
        if (CollectionUtils.isEmpty(skuAttrValueEntities)){
            return null;
        }

        // 转化成需要的数据接口
        //每个spu下有多个种类的sku，每个sku都有自己独特的配置（华为手机的sku：华为金色6+64，华为白色8+128 等等。。）
        Map<Long, List<SkuAttrValueEntity>> map = skuAttrValueEntities.stream().collect(Collectors.groupingBy(SkuAttrValueEntity::getAttrId));
        // 遍历map 把每个kv结构转化成{attrId: 3, attrName: 机身颜色, attrValues: ['暗夜黑', '白天白']}
        List<SaleAttrValueVo> saleAttrValueVos = new ArrayList<>();
        map.forEach((attrId, attrValues) -> {
            SaleAttrValueVo saleAttrValueVo = new SaleAttrValueVo();

            //attrId: 3
            saleAttrValueVo.setAttrId(attrId);
            // 只要有该分组，该分组下必然至少有一条记录，就从第一条中获取规格参数的名称
            //attrName: 机身颜色
            saleAttrValueVo.setAttrName(attrValues.get(0).getAttrName());
            // 把List<SkuAttrValueEntity> attrValues 转化成attrValues集合，最后输出Set集合
            //attrValues: ['暗夜黑', '白天白']
            saleAttrValueVo.setAttrValues(attrValues.stream().map(SkuAttrValueEntity::getAttrValue).collect(Collectors.toSet()));
            saleAttrValueVos.add(saleAttrValueVo);
        });
        return saleAttrValueVos;

    }

    @Override
    public Map<String, Long> queryMappingBySpuId(Long spuId) {
        // 根据spuId查询所有sku
        List<SkuEntity> skuEntities = this.skuMapper.selectList(new QueryWrapper<SkuEntity>().eq("spu_id", spuId));
        if (CollectionUtils.isEmpty(skuEntities)){
            return null;
        }

        // 集合所有的skuid
        List<Long> skuIds = skuEntities.stream().map(SkuEntity::getId).collect(Collectors.toList());

        List<Map<String, Object>> maps = this.attrValueMapper.queryMappingBySpuId(skuIds);
        if (CollectionUtils.isEmpty(maps)){
            return null;
        }

        // {sku_id=19, attrValues=金色,8G,512G}
        return maps.stream().collect(Collectors.toMap(map ->  map.get("attrValues").toString(), map -> (Long)map.get("sku_id")));
    }

    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<SkuAttrValueEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<SkuAttrValueEntity>()
        );

        return new PageResultVo(page);
    }

}