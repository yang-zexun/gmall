package com.atguigu.gmall.pms.service.impl;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.pms.entity.AttrEntity;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;
import com.atguigu.gmall.pms.entity.SpuAttrValueEntity;
import com.atguigu.gmall.pms.mapper.AttrMapper;
import com.atguigu.gmall.pms.mapper.SkuAttrValueMapper;
import com.atguigu.gmall.pms.mapper.SpuAttrValueMapper;
import com.atguigu.gmall.pms.vo.AttrValueVo;
import com.atguigu.gmall.pms.vo.GroupVo;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.pms.mapper.AttrGroupMapper;
import com.atguigu.gmall.pms.entity.AttrGroupEntity;
import com.atguigu.gmall.pms.service.AttrGroupService;
import org.springframework.util.CollectionUtils;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupMapper, AttrGroupEntity> implements AttrGroupService {
    @Autowired
    AttrMapper attrMapper;

    @Autowired
    private SpuAttrValueMapper baseAttrMapper;

    @Autowired
    private SkuAttrValueMapper saleAttrMapper;


    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<AttrGroupEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageResultVo(page);
    }

    @Override
    public List<AttrGroupEntity> getCategoryByCid(Long id) {
        QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id", id);
        List<AttrGroupEntity> list = this.list(wrapper);
        return list;
    }

    @Override
    public List<AttrGroupEntity> queryGroupsWithAttrsByCid(Long cid) {
        //一个属性组中包含着很多属性：主体属性组=上市年份+产品名称+机身颜色
        //根据category_id 查询对应的属性组 attr_group，然后给每个属性组的List集合填充对应的小属性
        List<AttrGroupEntity> attrGroupEntityList = this.list(new QueryWrapper<AttrGroupEntity>().eq("category_id", cid));
        if (CollectionUtils.isEmpty(attrGroupEntityList)) {
            return null;
        }

        //遍历属性组，然后给每个属性组的List集合填充对应的小属性
        attrGroupEntityList.forEach(attrGroupEntity -> {
            attrGroupEntity.setAttrEntities(attrMapper.selectList(new QueryWrapper<AttrEntity>()
                    .eq("group_id", attrGroupEntity.getId())
                    .eq("type", 1)));
        });

        return attrGroupEntityList;
    }

    @Override
    public List<GroupVo> queryGroupWithAttrValuesByCidAndSpuIdAndSkuId(Long cid, Long spuId, Long skuId) {
        // 1.查询当前商品所属分类下有哪些规格参数分组
        List<AttrGroupEntity> groupEntities = this.list(new QueryWrapper<AttrGroupEntity>().eq("category_id", cid));
        if (CollectionUtils.isEmpty(groupEntities)){
            return null;
        }
        // 2.遍历group集合获取每个分组下的规格参数
        return groupEntities.stream().map(attrGroupEntity -> {
            GroupVo groupVo = new GroupVo();

            // 设置分组的id和name
            groupVo.setGroupId(attrGroupEntity.getId());
            groupVo.setName(attrGroupEntity.getName());

            // 3.查询每个组下的规格参数
            List<AttrEntity> attrEntities = this.attrMapper.selectList(new QueryWrapper<AttrEntity>().eq("group_id", attrGroupEntity.getId()));
            if (CollectionUtils.isEmpty(attrEntities)){
                return groupVo;
            }
            // 获取attrIds集合
            List<Long> attrIds = attrEntities.stream().map(AttrEntity::getId).collect(Collectors.toList());

            List<AttrValueVo> attrs = new ArrayList<>();
            // 4.查询基本类型的规格参数及值
            List<SpuAttrValueEntity> spuAttrValueEntities = this.baseAttrMapper.selectList(new QueryWrapper<SpuAttrValueEntity>().eq("spu_id", spuId).in("attr_id", attrIds));
            if (!CollectionUtils.isEmpty(spuAttrValueEntities)){
                // 需要把List<SpuAttrValueEntity>转换成List<AttrValueVo>，然后把转化后的集合添加到attrs中
                attrs.addAll(spuAttrValueEntities.stream().map(spuAttrValueEntity -> {
                    AttrValueVo attrValueVo = new AttrValueVo();
                    BeanUtils.copyProperties(spuAttrValueEntity, attrValueVo);
                    return attrValueVo;
                }).collect(Collectors.toList()));
            }

            // 5.查询销售类型的规格参数及值
            List<SkuAttrValueEntity> skuAttrValueEntities = this.saleAttrMapper.selectList(new QueryWrapper<SkuAttrValueEntity>().eq("sku_id", skuId).in("attr_id", attrIds));
            if (!CollectionUtils.isEmpty(skuAttrValueEntities)){
                // 需要把List<skuAttrValueEntities>转换成List<AttrValueVo>，然后把转化后的集合添加到attrs中
                attrs.addAll(skuAttrValueEntities.stream().map(skuAttrValueEntity -> {
                    AttrValueVo attrValueVo = new AttrValueVo();
                    BeanUtils.copyProperties(skuAttrValueEntity, attrValueVo);
                    return attrValueVo;
                }).collect(Collectors.toList()));
            }
            groupVo.setAttrs(attrs);

            return groupVo;
        }).collect(Collectors.toList());
    }

}