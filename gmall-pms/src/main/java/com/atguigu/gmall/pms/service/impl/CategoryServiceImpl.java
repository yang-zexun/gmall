package com.atguigu.gmall.pms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.pms.mapper.CategoryMapper;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import com.atguigu.gmall.pms.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, CategoryEntity> implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<CategoryEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageResultVo(page);
    }

    @Override
    public List<CategoryEntity> getParentList(Long id) {
        QueryWrapper<CategoryEntity> wrapper = new QueryWrapper<>();
        if (id.equals(-1L)) {
            //查询所有
            return this.list();
        } else {
            //查询一级节点
            wrapper.eq("parent_id", id);
            return this.list(wrapper);
        }
    }

    @Override
    public List<CategoryEntity> queryCategoriesByPid(Long pid) {
        return this.list(new QueryWrapper<CategoryEntity>().eq("parent_id",pid));
    }

    @Override
    public List<CategoryEntity> queryLvl2WithSubsByPid(Long pid) {
        List<CategoryEntity> list = categoryMapper.queryLvl2WithSubsByPid(pid);
        return list;
    }

    @Override
    public List<CategoryEntity> queryLvl123CategoriesByCid3(Long cid3) {
        //先查询三级分类
        CategoryEntity categoryEntity3 = this.getById(cid3);
        if (categoryEntity3 == null){
            return null;
        }
        //查询二级分类
        CategoryEntity categoryEntity2 = this.getById(categoryEntity3.getParentId());
        //查询一级分类
        CategoryEntity categoryEntity1 = this.getById(categoryEntity2.getParentId());
        return Arrays.asList(categoryEntity1,categoryEntity2,categoryEntity3);
    }

}