package com.atguigu.gmall.pms.vo;

import com.atguigu.gmall.pms.entity.SpuEntity;
import lombok.Data;

import java.util.List;

@Data
public class SpuVo extends SpuEntity {
    // 海报图片
    private List<String> spuImages;

    // 基本属性
    //相比SpuEntity多了个月份信息，所以创建一个SpuAttrValueVo扩展一个月份属性
    private List<SpuAttrValueVo> baseAttrs;

    // sku列表
    private List<SkuVo> skus;
}
