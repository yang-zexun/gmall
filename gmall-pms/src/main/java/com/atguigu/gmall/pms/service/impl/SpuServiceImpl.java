package com.atguigu.gmall.pms.service.impl;

import com.atguigu.gmall.pms.entity.*;
import com.atguigu.gmall.pms.feign.GmallSmsClient;
import com.atguigu.gmall.pms.mapper.*;
import com.atguigu.gmall.pms.service.SkuAttrValueService;
import com.atguigu.gmall.pms.service.SkuImagesService;
import com.atguigu.gmall.pms.vo.SkuVo;
import com.atguigu.gmall.pms.vo.SpuAttrValueVo;
import com.atguigu.gmall.pms.vo.SpuVo;
import com.atguigu.gmall.sms.vo.SkuSaleVo;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.pms.service.SpuService;
import org.springframework.util.CollectionUtils;

@Slf4j
@Service("spuService")
public class SpuServiceImpl extends ServiceImpl<SpuMapper, SpuEntity> implements SpuService {

    @Autowired
    SpuDescMapper spuDescMapper;
    @Autowired
    SpuAttrValueServiceImpl spuAttrValueService;
    @Autowired
    SkuMapper skuMapper;
    @Autowired
    SkuImagesService skuImagesService;
    @Autowired
    SkuAttrValueService skuAttrValueService;
    @Autowired
    GmallSmsClient gmallSmsClient;
    @Autowired
    RabbitTemplate rabbitTemplate;

    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<SpuEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<SpuEntity>()
        );

        return new PageResultVo(page);
    }

    @Override
    public PageResultVo querySpuInfo(PageParamVo paramVo, Long categoryId) {
        QueryWrapper<SpuEntity> wrapper = new QueryWrapper<>();
        if (categoryId != 0) {
            //如果分类id不等于0，则按照分类id查，等于0就查全部
            wrapper.eq("category_id", categoryId);
        }
        String key = paramVo.getKey();
        //判断key是不是空的或者空格
        if (StringUtils.isNoneBlank(key)) {
            wrapper.and(t -> t.like("id", key).or().like("name", key));
        }
        IPage<SpuEntity> page = this.page(paramVo.getPage(), wrapper);
        return new PageResultVo(page);
    }

    @GlobalTransactional
    @Override
    public void bigSave(SpuVo spu) {
        /**
         * 保存psm_spu：标准单元基本信息
         * 保存psm_spu_desc：海报图片
         * 保存pms_spu_attr_value：标准单元的属性
         */
        //1.保存spu（标准单元基本信息）
        spu.setCreateTime(new Date());
        spu.setUpdateTime(spu.getCreateTime());
        this.save(spu);
        Long spuId = spu.getId();
        //2.保存spu_desc（海报图片）
        List<String> images = spu.getSpuImages();
        if (!CollectionUtils.isEmpty(images)) {
            SpuDescEntity spuDescEntity = new SpuDescEntity();
            spuDescEntity.setSpuId(spuId);
            spuDescEntity.setDecript(StringUtils.join(images, ","));
            spuDescMapper.insert(spuDescEntity);
        }
        //保存pms_spu_attr_value（标准单元的属性）
        List<SpuAttrValueVo> baseAttrs = spu.getBaseAttrs();
        //把基本属性的Vo集合转换完Entitle集合
        if (!CollectionUtils.isEmpty(baseAttrs)) {
            List<SpuAttrValueEntity> spuAttrValueEntities =
                    baseAttrs.stream().map(spuAttrValueVo -> {
                        SpuAttrValueEntity spuAttrValueEntity = new SpuAttrValueEntity();
                        BeanUtils.copyProperties(spuAttrValueVo, spuAttrValueEntity);
                        spuAttrValueEntity.setSpuId(spuId);
                        return spuAttrValueEntity;
                    }).collect(Collectors.toList());
            this.spuAttrValueService.saveBatch(spuAttrValueEntities);
        }

        /**
         * 保存psm_sku：商品基本信息
         * 保存psm_sku_desc：商品海报图片
         * 保存pms_sku_attr_value：商品的销售属性（购买时勾选的各种配置属性）
         */

        List<SkuVo> skus = spu.getSkus();
        if (CollectionUtils.isEmpty(skus)) {
            return;
        }
        skus.forEach(skuVo -> {
            //1.保存psm_sku：商品基本信息
            skuVo.setSpuId(spuId);
            skuVo.setCategoryId(spu.getCategoryId());
            skuVo.setBrandId(spu.getBrandId());
            // 获取图片列表，取第一张作为默认图片
            List<String> imageList = skuVo.getImages();
            if (!CollectionUtils.isEmpty(imageList)) {
                skuVo.setDefaultImage(StringUtils.isBlank(skuVo.getDefaultImage()) ? imageList.get(0) : skuVo.getDefaultImage());
            }
            skuMapper.insert(skuVo);
            Long skuId = skuVo.getId();

            //2.保存psm_sku_desc：商品海报图片
            if (!CollectionUtils.isEmpty(imageList)) {
                List<SkuImagesEntity> imagesEntities = imageList.stream().map(image -> {
                    //创建一个sku图片的对象
                    SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                    //封装sku的id
                    skuImagesEntity.setSkuId(skuId);
                    //封装sku商品的图片
                    skuImagesEntity.setUrl(image);
                    //有没有默认图片
                    skuImagesEntity.setDefaultStatus(StringUtils.equals(skuVo.getDefaultImage(), image) ? 1 : 0);
                    return skuImagesEntity;
                }).collect(Collectors.toList());
                skuImagesService.saveBatch(imagesEntities);
            }

            //3.保存pms_sku_attr_value：商品的销售属性（购买时勾选的各种配置属性）
            List<SkuAttrValueEntity> saleAttrs = skuVo.getSaleAttrs();
            if (!CollectionUtils.isEmpty(saleAttrs)) {
                saleAttrs.forEach(saleAttr -> {
                    saleAttr.setSkuId(skuId);
                });
                skuAttrValueService.saveBatch(saleAttrs);
            }


            /**
             * 保存Sku对象中的营销信息，远程调用
             */
            SkuSaleVo skuSaleVo = new SkuSaleVo();
            BeanUtils.copyProperties(skuVo, skuSaleVo);
            skuSaleVo.setSkuId(skuId);
            gmallSmsClient.saveSkuSaleInfo(skuSaleVo);

            //大保存方法提交前，发送mq
            try {
                this.rabbitTemplate.convertAndSend("PMS_SPU_EXCHANGE","item.insert",spuId);
            } catch (AmqpException e) {
                log.error("mq消息发送失败，交换机：{}，商品id：{}","PMS_SPU_EXCHANGE",spuId);
            }
        });
    }
}

