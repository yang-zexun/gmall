package com.atguigu.gmall.ums.controller;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.ums.entity.UserAddressEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class UserAddressControllerTest {
    @Autowired
    UserAddressController addressController;
    @Test
    void testQueryUserAddressByPage(){
        ResponseVo<UserAddressEntity> vo = this.addressController.queryUserAddressById(2L);
        UserAddressEntity data = vo.getData();
        System.out.println(data.getAddress());
    }
}