package com.atguigu.gmall.ums.service.impl;

import com.atguigu.gmall.common.utils.RsaUtils;
import org.apache.commons.codec.cli.Digest;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.ums.mapper.UserMapper;
import com.atguigu.gmall.ums.entity.UserEntity;
import com.atguigu.gmall.ums.service.UserService;
import org.springframework.util.CollectionUtils;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<UserEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<UserEntity>()
        );

        return new PageResultVo(page);
    }

    @Override
    public void register(UserEntity userEntity, String code) {
        // TODO：1.校验验证码：根据手机号查询redis中的code  和 页面的code 比较

        //生成盐，这个盐后续查询时 依然会使用该盐对用户输入的明文密码 进行加盐加密
        String salt = StringUtils.substring(UUID.randomUUID().toString(), 0, 6);
        userEntity.setSalt(salt);

        // 3.加盐加密 然后使用密文密码把明文密码覆盖
        userEntity.setPassword(DigestUtils.md5Hex(userEntity.getPassword()+salt));

        // 4.新增用户
        userEntity.setLevelId(1l);
        userEntity.setNickname(userEntity.getUsername());
        userEntity.setSourceType(1);
        userEntity.setIntegration(1000);
        userEntity.setGrowth(1000);
        userEntity.setStatus(1);
        userEntity.setCreateTime(new Date());
        this.save(userEntity);

        // TODO: 删除redis中的验证码
    }

    @Override
    public Boolean checkData(String data, Integer type) {
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<UserEntity>();
        switch (type){
            case 1: wrapper.eq("username", data); break;
            case 2: wrapper.eq("phone", data); break;
            case 3: wrapper.eq("email", data); break;
            default:
                return null;
        }
        return this.count(wrapper) == 0;
    }

    @Override
    public UserEntity queryUser(String loginName, String password) {
        // 根据登录名查询，查询出符合登录名的用户
        // 可能查出来很多个用户，因为email和phone可能会被当成username来注册
        List<UserEntity> userEntities = this.list(new QueryWrapper<UserEntity>().eq("username", loginName)
                .or().eq("email", loginName)
                .or().eq("phone", loginName));

        if(CollectionUtils.isEmpty(userEntities)){
            return null; // 用户名输入不合法
        }

        // 遍历用户集合，获取每个用户的盐，与输入的密码进行加盐加密，

        for (UserEntity userEntity : userEntities) {
            String s = DigestUtils.md5Hex(password + userEntity.getSalt());
            // 然后和遍历中的用户密码比较，如果相同，就说明是这个用户
            if (StringUtils.equals(s,userEntity.getPassword())){
                return userEntity;
            }
        }
        return null;
    }

}