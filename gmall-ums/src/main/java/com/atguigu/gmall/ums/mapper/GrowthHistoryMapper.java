package com.atguigu.gmall.ums.mapper;

import com.atguigu.gmall.ums.entity.GrowthHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 成长积分记录表
 * 
 * @author jbyf
 * @email jbyf@atguigu.com
 * @date 2021-08-16 10:34:57
 */
@Mapper
public interface GrowthHistoryMapper extends BaseMapper<GrowthHistoryEntity> {
	
}
