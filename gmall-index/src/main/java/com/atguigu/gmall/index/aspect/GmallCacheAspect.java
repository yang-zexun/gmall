package com.atguigu.gmall.index.aspect;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.index.annotation.GmallCache;
import com.atguigu.gmall.index.feign.GmallPmsClient;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import com.google.common.hash.BloomFilter;
import org.aopalliance.intercept.Joinpoint;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class GmallCacheAspect {
    @Autowired
    RedissonClient redissonClient;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    RBloomFilter bloomFilter;

    @Autowired
    GmallPmsClient pmsClient;

    @Around("@annotation(com.atguigu.gmall.index.annotation.GmallCache)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable{
        // 获取签名(强转成MethodSignature)
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        // 获取目标方法对象
        Method method = signature.getMethod();
        // 获取@GmallCache注解，以便获取注解里面设定的参数
        GmallCache gmallCache = method.getAnnotation(GmallCache.class);

        //获取注解中的缓存前缀
        String prefix = gmallCache.prefix();
        //获取参数列表
        String join = StringUtils.join(joinPoint.getArgs(), ",");
        //组装缓存的key值
        String key = prefix + join;
        // 使用布隆过滤器，判定数据是否存在，如果不存在，直接返回空
        if (!bloomFilter.contains(key)) {
            return null;
        }

        // 1.先查缓存，如果命中，直接返回
        String json = this.redisTemplate.opsForValue().get(key);
        if (StringUtils.isNoneBlank(json)){
            return JSON.parseArray(json, CategoryEntity.class);
        }

        // 2.缓存中如果没有，调用目标方法自己的查询方法查询，并存入缓存
            // 2.1 防止缓存击穿，添加分布式锁
        RLock lock = redissonClient.getLock(gmallCache.lock()+join);
        lock.lock();
        try {
            // 2.2 放一个请求拿到锁过去查到数据并放入缓存后，后续的请求获取锁之后应该再次确认以下缓存库是否存在数据
            String json1 = this.redisTemplate.opsForValue().get(key);
            if (StringUtils.isNoneBlank(json1)){
                return JSON.parseArray(json1,CategoryEntity.class);
            }
            //2.3 执行目标的查询方法，并存入redis
            Object result = joinPoint.proceed(joinPoint.getArgs());
            if (result != null){
                //设置缓存过期时间，加随机值，防止缓存雪崩
                int timeout = gmallCache.timeout() + new Random().nextInt(gmallCache.random());
                this.redisTemplate.opsForValue().set(key,JSON.toJSONString(result),timeout, TimeUnit.MINUTES);
            }

            return result;
        } finally {
            lock.unlock();
        }


    }
}
