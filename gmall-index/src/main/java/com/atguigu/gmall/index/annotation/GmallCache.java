package com.atguigu.gmall.index.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GmallCache {
    //缓存的前缀
    String prefix() default "";

    //缓存的有效时间，分钟
    int timeout() default 30;

    //防止缓存雪崩，随机范围
    int random() default 10;

    //分布式锁的前缀：
    String lock() default "lock:";


}
